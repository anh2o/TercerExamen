package com.example.BlogRestaurante.entities;
import org.hibernate.validator.constraints.NotEmpty;
import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "pedido")
public class Pedido {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "nombredepedido")
    private String nombre_Pedido;
    @Column(name = "preciopedido")
    private Integer precio_Pedido;

    @Column(name = "direccion")
    private String direccion;

    /**EXAMEN DE TALLER DE PROGRAMACION**/
    @NotNull
    @NotEmpty(message = "*Por favor ingrese su calle")
    @Column(name = "calle")
    private String calle;

    @NotNull
    //@NotEmpty(message = "*Por favor ingrese su nro de casa")
    @Column(name = "nroCasa")
    private Integer nroCasa;

    @NotNull
    @NotEmpty(message = "*Por favor ingrese su zona")
    @Column(name = "zona")
    private String zona;

   //@NotEmpty(message = "*Por favor ingrese su codigo zip")
   //@Size(min = 1,max = 5,message = "Debe ser mayor a 1 y menor a 6 digitos")
    @Column(name = "zipCode")
    private Integer zipCode;


    @ManyToOne
    @JoinColumn(name = "category_restaurant")
    private Restaurant category_restaurant = null;

    @ManyToOne
    @JoinColumn(name = "category_choice")
    private Choice category_choice;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User id_user;

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public Pedido(User user){
        this.id_user=user;
    }

    public Pedido(){};

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre_Pedido() {
        return nombre_Pedido;
    }

    public void setNombre_Pedido(String nombre_Pedido) {
        this.nombre_Pedido = nombre_Pedido;
    }

    public Integer getPrecio_Pedido() {
        return precio_Pedido;
    }

    public void setPrecio_Pedido(Integer precio_Pedido) {
        this.precio_Pedido = precio_Pedido;
    }

    public Restaurant getCategory_restaurant() {
        return category_restaurant;
    }

    public void setCategory_restaurant(Restaurant category_restaurant) {
        this.category_restaurant = category_restaurant;
    }


    public Choice getCategory_choice() {
        return category_choice;
    }

    public void setCategory_choice(Choice category_choice) {
        this.category_choice = category_choice;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    public User getId_user() {
        return id_user;
    }

    public void setId_user(User id_user) {
        this.id_user = id_user;
    }
}
